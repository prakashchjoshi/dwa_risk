<?php
echo $this->html->css('lightbox.css');
echo $this->html->script('lightbox.js');
?>


<?php if(!$loggedIn) { ?>

     <ul class="breadcrum"><li><?php echo $this->Html->addCrumb('About Us >>','/Abouts/generalaboutushtml'); ?>       
     &nbsp;Gallery</li></ul>
  
 <?php }else{ 
 echo $this->Html->addCrumb('Gallery', '/Galleries');
 }?>
<div id="container-wrapper">

<div class="Galleries index">
    <div class="imageRow">
  	<div class="set">
  	  <div class="single first">
  		  <a href="gallery/image-1.jpg" rel="lightbox[plants]" title="Click on the right side of the image to move forward."><img src="gallery/thumb-1.jpg" alt="Plants: image 1 0f 4 thumb" width="140" height="140" /></a>
  		</div>
      <div class="single">
  		  <a href="gallery/image-2.jpg" rel="lightbox[plants]" title="Alternately you can press the right arrow key." ><img src="gallery/thumb-2.jpg" alt="Plants: image 2 0f 4 thumb" width="140" height="140" /></a>
      </div>
      <div class="single">
  		  <a href="gallery/image-3.jpg" rel="lightbox[plants]" title="The script preloads the next image in the set as you're viewing."><img src="gallery/thumb-3.jpg" alt="Plants: image 3 0f 4 thumb" width="140" height="140" /></a>
      </div>
      <div class="single">
  		  <a href="gallery/image-4.jpg" rel="lightbox[plants]" title="The script preloads the next image in the set as you're viewing."><img src="gallery/thumb-4.jpg" alt="Plants: image 3 0f 4 thumb" width="140" height="140" /></a>
      </div>
      <div class="single">
  		  <a href="gallery/image-5.jpg" rel="lightbox[plants]" title="The script preloads the next image in the set as you're viewing."><img src="gallery/thumb-5.jpg" alt="Plants: image 3 0f 4 thumb" /></a>
      </div>
      <div class="single last">
  		  <a href="gallery/image-6.jpg" rel="lightbox[plants]" title="Click the X or anywhere outside the image to close"><img src="gallery/thumb-6.jpg" alt="Plants: image 4 0f 4 thumb" /></a>
      </div>
  	</div>
  </div>
  <div class="imageRow">
  	<div class="set">
  	  <div class="single first">
  		  <a href="gallery/image-7.jpg" rel="lightbox[plants]" title="Click on the right side of the image to move forward."><img src="gallery/thumb-7.jpg" alt="Plants: image 1 0f 4 thumb" width="140" height="140" /></a>
  		</div>
      <div class="single">
  		  <a href="gallery/image-8.jpg" rel="lightbox[plants]" title="Alternately you can press the right arrow key." ><img src="gallery/thumb-8.jpg" alt="Plants: image 2 0f 4 thumb" width="140" height="140" /></a>
      </div>
      <div class="single">
  		  <a href="gallery/image-9.jpg" rel="lightbox[plants]" title="The script preloads the next image in the set as you're viewing."><img src="gallery/thumb-9.jpg" alt="Plants: image 3 0f 4 thumb" width="140" height="140" /></a>
      </div>
      <div class="single">
  		  <a href="gallery/image-10.jpg" rel="lightbox[plants]" title="The script preloads the next image in the set as you're viewing."><img src="gallery/thumb-10.jpg" alt="Plants: image 3 0f 4 thumb" width="140" height="140" /></a>
      </div>
      <div class="single">
  		  <a href="gallery/image-11.jpg" rel="lightbox[plants]" title="The script preloads the next image in the set as you're viewing."><img src="gallery/thumb-11.jpg" alt="Plants: image 3 0f 4 thumb" width="150" height="150" /></a>
      </div>
      <div class="single last">
  		  <a href="gallery/image-12.jpg" rel="lightbox[plants]" title="Click the X or anywhere outside the image to close"><img src="gallery/thumb-12.jpg" alt="Plants: image 4 0f 4 thumb" /></a>
      </div>
  	</div>
  </div>
    </div>
</div>   
</div>

<!--<div class="wrapper col4">
  <div id="container">
    <div id="leftbody"> <a href="#"><img src="images/demo/200x1404.gif" alt="" /> <br class="clear" />
    </div>
    <div id="rightbody">
      <h1>Location On Map</h1>
      <p> <div style="width: 600px"><iframe width="600" height="300" src="http://regiohelden.de/google-maps/map_en.php?width=600&amp;height=300&amp;hl=en&amp;q=university%20of%20juba%20sudan+(RegioHelden%20GmbH)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=A&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="http://www.regiohelden.de/google-maps/">Google Maps Generator</a> von <a href="http://www.regiohelden.de/">RegioHelden</a></iframe><br /><span style="font-size: 9px;"><a href="http://www.regiohelden.de/google-maps/" style="font-size: 9px;">Google Maps Generator</a> by <a href="http://www.regiohelden.de/" style="font-size: 9px;">RegioHelden</a></span></div></p>
      <br class="clear" />
    </div>
    <br class="clear" />
  </div>
</div>-->

   <script>

  jQuery(document).ready(function($) {
      $('a').smoothScroll({
        speed: 1000,
        easing: 'easeInOutCubic'
      });

      $('.showOlderChanges').on('click', function(e){
        $('.changelog .old').slideDown('slow');
        $(this).fadeOut();
        e.preventDefault();
      });
  });
</script>
