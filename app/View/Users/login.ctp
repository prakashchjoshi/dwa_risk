<?php ?>
<div class="wrapper col3">
<div class="SliderBg"> 
    <!-- Begin Slider -->
    <div id="nivo-slider"> 
    <img src="<?php echo $this->webroot; ?>img/slider/slide1.jpg" width="952" height="313" alt="img1" title="Education is power " /> 
    <img src="<?php echo $this->webroot; ?>img/slider/slide2.jpg" width="952" height="313" alt="img2" title="Education is power" /> 
    <img src="<?php echo $this->webroot; ?>img/slider/slide3.jpg" width="952" height="313" alt="img4" title="Education is power" /> 
    <img src="<?php echo $this->webroot; ?>img/slider/slide4.jpg" width="952" height="313" alt="img5" title="Education is power " /> 
    <img src="<?php echo $this->webroot; ?>img/slider/slide5.jpg" width="952" height="313" alt="img5" title="Education is power" /> 
    <img src="<?php echo $this->webroot; ?>img/slider/slide6.jpg" width="952" height="313" alt="img5" title="Education is power " /> 
    <img src="<?php echo $this->webroot; ?>img/slider/slide7.jpg" width="952" height="313" alt="img5" title="Education is power" /> 
    <img src="<?php echo $this->webroot; ?>img/slider/slide8.jpg" width="952" height="313" alt="img5" title="Education is power" />
    <img src="<?php echo $this->webroot; ?>img/slider/slide9.jpg" width="952" height="313" alt="img5" title="Education is power" />
    </div>
    <!-- End Slider --> 
  </div>
  </div>
  
  <div id="container-wrapper" class="col4">      
    <div id="hpage">
      <ul>
        <li>
          <h2>About MHERST</h2>
          <div class="imgholder"><a href="#"><img src="<?php echo $this->webroot; ?>img/demo/200x1401.gif" alt="" /></a></div>
          <p style="text-align:justify">The Ministry for Higher Eduction, Science and Technology [MoHEST] was established by President Degree Number 62/2010 of 21st June in accordance with the provision of Articles 55,56 and 103[1] of the interim Contiution of Southern Sudan [ICSS], 2005 read in conjunction with Schedule [B] of the interim Contiution of The Repulbic of Sudan. By this decree all functions, assets and personnel of the former Directorate of Higher and Teritiary Eduction in the Ministry of Eduction, Science and Technology [MoHEST].</p>
          <p class="readmore"><a href="<?php echo $this->webroot?>Abouts/generalaboutushtml">Continue Reading &raquo;</a></p>
        </li>
        <li>
          <h2>Library MHERST</h2>
          <div class="imgholder"><a href="#"><img src="<?php echo $this->webroot; ?>img/demo/200x1402.gif" alt="" /></a></div>
          <p style="text-align:justify">Coming Soon...</p>
          <p class="readmore"><a href="#">Continue Reading &raquo;</a></p>
        </li>
        <li>
          <h2>Study at MHERST</h2>
          <div class="imgholder"><a href="#"><img src="<?php echo $this->webroot; ?>img/demo/200x1403.gif" alt="" /></a></div>
          <p style="text-align:justify">Coming Soon...</p>
          <p class="readmore"><a href="#">Continue Reading &raquo;</a></p>
        </li>
        <li class="last">
          <h2>Activities MHERST</h2>
          <div class="imgholder"><a href="#"><img src="<?php echo $this->webroot; ?>img/demo/200x1404.gif" alt="" /></a></div>
          <p style="text-align:justify">Coming Soon...</p>
          <p class="readmore"><a href="#">Continue Reading &raquo;</a></p>
        </li>
      </ul>
      <br class="clear" />
    </div>
</div>
