<?php

if (!empty($colleges) || !empty($selectedCollegeData)) {
    if (empty($admission_type)) {
        $option = '<table border="0" style="margin:auto;" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top" style="width:37%;">
	<div class="first-block"><select name="left-select" id="left-select" size="5" style="min-height:100px;">
	';
        if (!empty($colleges)) {
            foreach ($colleges as $college) {
                $isvalid = true;

                foreach ($selectedCollegeData as $selected) {
                    if (!empty($selected['CollegeGroupSubject']['id'])) {
                        if ($selected['CollegeGroupSubject']['id'] == $college['CollegeGroupSubject']['id']) {
                            $isvalid = false;
                            break;
                        }
                    }
                }
                if ($isvalid)
                    $option .= "<option value='{$college['CollegeGroupSubject']['id']}','rel'=''>{$college['College']['University']['name']}  {$college['College']['college_code']}  {$college['GroupSubjects']['name']}</option>";
            }
        }
        $option .='</div></td>
    <td align="left" valign="top" style="width:15%;">
	<div class="mid-block">
	
    <a onclick="sendSelectedOptions();" style="cursor:pointer;"> <img src=' . $this->webroot . 'img/swap-right.png /></a>
    <br/><a onclick="removeSelectedOptions();" style="cursor:pointer;"> <img src=' . $this->webroot . 'img/swap-left.png /> </a></div>
    </td>
     <td align="left" valign="top" style="width:33%;">
	 <div class="last-block"><select name="right-select[]" id="right-select" multiple="multiple" size="5"  style="min-height:100px;">';

        if (!empty($selectedCollegeData)) {
            foreach ($selectedCollegeData as $selected) {
                if (!empty($selected['CollegeGroupSubject']['id'])) {
                    $option .= "<option selected='selected' value='{$selected['CollegeGroupSubject']['id']}','rel'=''>{$selected['CollegeGroupSubject']['College']['University']['name']}  {$selected['CollegeGroupSubject']['College']['college_code']}  {$selected['CollegeGroupSubject']['GroupSubjects']['name']}</option>";
                }
            }
        }

        $option .='</select></div>
    </td>
  </tr>
</table>';

        echo $option;
        exit;
    } elseif ($admission_type == 'P') {


        $option = '<select name="right-select[]" id="privateStudentcollege" style="width:21%!important;">';

        if (!empty($colleges)) {
            foreach ($colleges as $privatecollegeoption) {
                $isvalid = true;

                if (!empty($selectedCollegeData[0]['CollegeGroupSubject']['id'])) {
                    foreach ($selectedCollegeData as $selected) {
                        if ($selected['CollegeGroupSubject']['id'] == $privatecollegeoption['CollegeGroupSubject']['id']) {
                            $option .= "<option selected='selected' value='{$privatecollegeoption['CollegeGroupSubject']['id']}'>{$privatecollegeoption['College']['University']['name']}  {$privatecollegeoption['College']['college_code']}  {$privatecollegeoption['GroupSubjects']['name']}</option>";
                        }
                    }
                }
                $option .= "<option value='{$privatecollegeoption['CollegeGroupSubject']['id']}'>{$privatecollegeoption['College']['University']['name']}  {$privatecollegeoption['College']['college_code']}  {$privatecollegeoption['GroupSubjects']['name']}</option>";
            }
        }
        $option .= "</select>";
        echo $option;
        exit;
    }
} else {
    $option = '<table border="0" style="margin:auto;" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top" style="width:37%;color:red;font-size:10pt;">*No College found for these subjects match. Please enter your right combinational subjects</td></tr></table>';

    echo $option;
    exit;
}
?>
