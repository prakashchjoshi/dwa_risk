<div class="groupSubjects form">
<?php  echo $this->Html->addCrumb('Group Subject', '/GroupSubjects'); ?>
    <?php echo $this->Form->create('GroupSubject'); ?>
    <fieldset>
        <legend><?php echo __('Add Group Subject'); ?></legend>
        <?php
        $options = array('N' => 'No', 'Y' => 'Yes');
        echo $this->Form->input('name', array('required' => false));
        echo $this->Form->input('status', array('type' => 'radio', 'class' => 'radio', 'options' => $options, 'checked' => 'checked'));
        ?> 
        <div class="subject-multi"> 
            <?php echo $this->Form->input('GroupSubSubject.subject_id', array('type' => 'select', 'multiple' => 'checkbox', 'options' => $subjects));
            ?>
        </div>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>

        <li><?php echo $this->Html->link(__('List Group Subjects'), array('action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('List Group Sub Subjects'), array('controller' => 'group_sub_subjects', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List College Group Subjects'), array('controller' => 'college_group_subjects', 'action' => 'index')); ?> </li>
       </ul>
</div>
