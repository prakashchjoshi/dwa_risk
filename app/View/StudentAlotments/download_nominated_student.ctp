<div class="studentAlotments index">
    <?php  echo $this->Html->addCrumb("Student Alotment", '/StudentAlotments'); ?>

	<h2><?php echo __('Student Alotments'); ?></h2>
	
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th>S.No</th>
			<th>Application Number</th>
			<th>College Name</th>
			
			<th>Rank(Grade)</th>
			<th>Allocation Year</th>
			<th>Created Date</th>
			
	</tr>
		<?php if(!empty($studentAlotments)): foreach ($studentAlotments as $studentAlotment): ?>
	<tr>
		<td><?php echo $studentAlotment['StudentAlotment']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $studentAlotment['StudentRegistration']['applicant_name']; ?>
		</td>
		
		<td>
  <?php echo $studentAlotment['CollegeGroupSubject']['GroupSubjects']['name']; ?>
		</td>
		
		<td><?php echo $studentAlotment['StudentAlotment']['grade']; ?>&nbsp;</td>
		<td><?php echo $studentAlotment['StudentAlotment']['allocation_year']; ?>&nbsp;</td>
		<td><?php echo date('d-m-Y',strtotime($studentAlotment['StudentAlotment']['created'])); ?>&nbsp;</td>
		
	</tr>
<?php endforeach; endif;?>
	</table>
	
</div>
