<div class="studentAlotments index">
<?php  echo $this->Html->addCrumb("Student Alotment", '/StudentAlotments'); ?>
	<h2><?php echo __('Exceptions'); ?></h2>
	
	<table cellpadding="0" cellspacing="0">
	<tr class="headblock">
			
			<th><?php echo 'University'; ?></th>
			<th><?php echo 'College'; ?></th>
			<th><?php echo 'Group Subject name'; ?></th>
			<th><?php echo 'Number of seat'; ?></th>
			<th><?php echo 'Actual Admission'; ?></th>
			
	</tr>
	<?php
	 $cssclass='';
	 foreach ($exceptionallotment as $exceptionallotment):
	 if(!empty($exceptionallotment['CollegeGroupSubject']['no_of_seat']) && $exceptionallotment['CollegeGroupSubject']['no_of_seat']<$exceptionallotment[0]['opt']):
	 $cssclass="class='exception'";
	 endif;
	  ?>
	<tr>
	<td><?php echo @$exceptionallotment['CollegeGroupSubject']['College']['University']['name']; ?>&nbsp;</td>	
<td><?php echo @$exceptionallotment['CollegeGroupSubject']['College']['name']
		; ?>&nbsp;</td>		
		<td><?php echo @$exceptionallotment['CollegeGroupSubject']['GroupSubjects']['name']; ?>&nbsp;</td>		
		<td <?php echo $cssclass;?>><?php echo @$exceptionallotment['CollegeGroupSubject']['no_of_seat']; ?>&nbsp;</td>
		<td <?php echo $cssclass;?>><?php echo @$exceptionallotment[0]['opt']; ?>&nbsp;</td>		
		
	</tr>
         <?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php //echo $this->Html->link(__('New Student Alotment'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Student Registrations'), array('controller' => 'student_registrations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Student Registration'), array('controller' => 'student_registrations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List College Group Subjects'), array('controller' => 'college_group_subjects', 'action' => 'index')); ?> </li>
		<li><?php //echo $this->Html->link(__('Nominated Student list'), array('action' => 'nominatedstudent')); ?> </li>
		<li><?php echo $this->Html->link(__('Last Nominated Percentage'), array('action' => 'listallocatedpercentage')); ?> </li>
		<li><?php echo $this->Html->link('Download Exceptions',array('action'=>'download_exceptions'))?></li>
		
	</ul>
</div>

