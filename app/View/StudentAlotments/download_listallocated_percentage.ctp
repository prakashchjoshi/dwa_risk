<div class="studentAlotments index">

	<h2><?php echo __('Percentage Of Last Nominated Students'); ?></h2>
	
	<table cellpadding="0" cellspacing="0">
	<tr>
	<th>College Name</th>
	<th>Subject Group</th>
	<th>Marks</th>
        <th>Allocation Year</th>
	</tr>
	<?php
	 foreach ($alllcatedpercentageDetails as $listpercentage): 
	?>
	 
		<tr>
		<td><?php echo trim($listpercentage['CollegeGroupSubject']['College']['name']); ?>&nbsp;</td>		
		<td><?php echo h($listpercentage['CollegeGroupSubject']['GroupSubjects']['name']); ?>&nbsp;</td>		
		<td><?php echo h($listpercentage[0]['total_percentage']); ?>&nbsp;</td>
		<td><?php echo h($listpercentage['StudentAlotment']['allocation_year']); ?>&nbsp;</td>		
		
	</tr>
<?php endforeach; ?>
	</table>
	
</div>
