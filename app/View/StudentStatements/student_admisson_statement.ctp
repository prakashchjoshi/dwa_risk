<?php echo $this->Html->addCrumb('Student Statement', '/StudentStatements/student_admisson_statement'); ?>

<div class="studentRegistrations form col4">
<h2>Student Statements</h2>

        <?php echo $this->Form->create('StudentRegistration',array('url'=>array('controller'=>'StudentStatements','action'=>'student_admisson_statement'))); ?>
        <fieldset>
        
        <div class="form-area2">
                <div class="left-area">
                <?php 
                 echo $this->Form->input('application_number', array('label'=>'Student Application Number','tabindex' => '1', 'id' => 'applicantnumber'));
                  
                ?><span id="message" style="color:red"></span>
                
                
                </div>
                <div class="right-area">
                <label>Student Name: </label>
                <span id="studentID"></span>
                </div>
                
                <div class="right-area">
                <label>Admission Year: </label>
                <span id="admission_year"></span>               
                </div>
                <div id="studentdetails">
               		<table>
					<tr>
						<th><?php echo "University"; ?></th>
						<th><?php echo "College"; ?></th>
						<th><?php echo "Course"; ?></th>
						<th><?php echo "Preference No"; ?></th>
						<th><?php echo "% of last student"; ?></th>
								
					</tr>

					<tr>
						<td><label id="univeristyname"></label></td>
						<td><label id="collegename"></label></td>
						<td><label id="groupsubjectname"></label></td>
						<td><label id="preferenceno"></label></td>
						<td><label id="laststdpercentage"></label></td>
								
					</tr>
					</table>
                </div>
                
                
                <div id="blankstd_details" style="display:none">
               		<table>
					<tr>
						<th><?php echo "University"; ?></th>
						<th><?php echo "College"; ?></th>
						<th><?php echo "Course"; ?></th>
						<th><?php echo "Preference No"; ?></th>
						<th><?php echo "% of last student"; ?></th>
								
					</tr>

					<tr>
						<td>Please enter right application number to view statement</td>
						
								
					</tr>
					</table>
                </div>
                                
                </div>
                
                </fieldset>
                
                
       
</div>

<?php //echo $this->element('sql_dump');?>

<script>
$(document).ready(function(){
$("#message").html("*Enter a valid application number above in the textbox");
});
$("#applicantnumber").blur(function(){
var appnum = $("#applicantnumber").val();


	if(appnum!='' && /^\d+$/.test(appnum)){
	$("#message").html('');
        $.ajax({
            type: 'POST',
            data: {applicationNum: $("#applicantnumber").val()},
            url: "<?php echo $this->webroot; ?>StudentStatements/getcourse_year",
            success: function(data) {
            
            
              var courseyear = $.parseJSON(data);
             
              $("#studentID").html(courseyear.studentID);
              $("#admission_year").html(courseyear.year);
            }
        });
        return true;
	
        }else{
        	
        	if(appnum=='' || appnum==null){
        	
        		$("#message").html("*Please Enter application number");

        		
        	}else{
		    	if(/^\d+$/.test(appnum)==false){
		    	$("#message").html("*Please Enter a valid application number");

		    	}
        	}
        	
        	
        	$("#studentID").html('');
            $("#admission_year").html('');	
           // $("#message").html("*Enter Application Number above in the textbox");
            $("#applicantnumber").focus()
        }


	
});

$("#applicantnumber").blur(function(){

	if($("#applicantnumber").val()!='' && $("#applicantnumber").val()!=null){
        $.ajax({
            type: 'POST',
            data: {applicationNum: $("#applicantnumber").val()},
            url: "<?php echo $this->webroot; ?>StudentStatements/getstudentdetails",
            success: function(data) {
            $("#blankstd_details").hide();
            $("#studentdetails").html(data);
            }
        });
        return true;
	
        }else{
        $("#blankstd_details").show();
        $("#studentdetails").html('');
        }


	
});
</script>

