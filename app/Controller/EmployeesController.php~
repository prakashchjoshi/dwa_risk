<?php

App::uses('AppController', 'Controller');

/**
 * Employees Controller
 *
 * @property Employee $Employee
 */
class EmployeesController extends AppController {

    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = array('Html', 'Form');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Employee->recursive = 0;
        $this->set('employees', $this->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Employee->exists($id)) {
            throw new NotFoundException(__('Invalid employee'));
        }
        $options = array('conditions' => array('Employee.' . $this->Employee->primaryKey => $id));
        $employeeData = $this->Employee->find('first', $options);
        $this->loadModel('User');
        $createUserId = $employeeData['Employee']['created_by'];
        $createUserData = $this->User->find('first', array('fields' => array('id', 'username'), 'condition' => array('User.id' => $createUserId)));
        $modifyUserId = $employeeData['Employee']['modified_by'];
        $employeeData['Employee']['created'] = date("d-m-Y", strtotime($employeeData['Employee']['created']));
        $employeeData['Employee']['modified'] = date("d-m-Y", strtotime($employeeData['Employee']['modified']));
        $modifyUserData = $this->User->find('first', array('fields' => array('id', 'username'), 'condition' => array('User.id' => $modifyUserId)));
        $this->set('employee', $employeeData);
        $this->set('createuser', $createUserData['User']['username']);
        $this->set('modifyuser', $modifyUserData['User']['username']);
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            if (!empty($this->request->data) && is_uploaded_file($this->request->data['Employee']['document']['tmp_name'])) {
                $fileData = fread(fopen($this->request->data['Employee']['document']['tmp_name'], "r"), $this->request->data['Employee']['document']['size']);
                $this->request->data['Employee']['document'] = $fileData;
            }  else {
                $this->request->data['Employee']['document'] = 'null';
            }
            $this->request->data['Employee']['date_of_joining'] = date("Y-m-d", strtotime($this->request->data['Employee']['date_of_joining']));
            $this->request->data['Employee']['date_of_confirm_joining'] = date("Y-m-d", strtotime($this->request->data['Employee']['date_of_confirm_joining']));
            $this->request->data['Employee']['date_of_resignation'] = date("Y-m-d", strtotime($this->request->data['Employee']['date_of_resignation']));
            $userid = $this->Session->read('Auth.User.id');
            $this->request->data['Employee']['created_by'] = $userid;
            $this->Employee->create();
            if ($this->Employee->save($this->request->data)) {
                $this->Session->setFlash(__('The employee has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The employee could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Employee->exists($id)) {
            throw new NotFoundException(__('Invalid employee'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if (!empty($this->request->data) && is_uploaded_file($this->request->data['Employee']['document']['tmp_name'])) {
                $fileData = fread(fopen($this->request->data['Employee']['document']['tmp_name'], "r"), $this->request->data['Employee']['document']['size']);
                $this->request->data['Employee']['document'] = $fileData;
            }else {
                $this->request->data['Employee']['document'] = 'null';
            }
            $this->request->data['Employee']['date_of_joining'] = date("Y-m-d", strtotime($this->request->data['Employee']['date_of_joining']));
            $this->request->data['Employee']['date_of_confirm_joining'] = date("Y-m-d", strtotime($this->request->data['Employee']['date_of_confirm_joining']));
            $this->request->data['Employee']['date_of_resignation'] = date("Y-m-d", strtotime($this->request->data['Employee']['date_of_resignation']));
            $userid = $this->Session->read('Auth.User.id');
            $this->request->data['Employee']['modified_by'] = $userid;
            if ($this->Employee->save($this->request->data)) {
                $this->Session->setFlash(__('The employee has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The employee could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Employee.' . $this->Employee->primaryKey => $id));
            $this->request->data = $this->Employee->find('first', $options);
            $this->request->data['Employee']['date_of_joining'] = date("d-m-Y", strtotime($this->request->data['Employee']['date_of_joining']));
            $this->request->data['Employee']['date_of_confirm_joining'] = date("d-m-Y", strtotime($this->request->data['Employee']['date_of_confirm_joining']));
            $this->request->data['Employee']['date_of_resignation'] = date("d-m-Y", strtotime($this->request->data['Employee']['date_of_resignation']));
            $this->set('editid', $this->request->data['Employee']['id']);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @throws MethodNotAllowedException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Employee->id = $id;
        if (!$this->Employee->exists()) {
            throw new NotFoundException(__('Invalid employee'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Employee->delete()) {
            $this->Session->setFlash(__('Employee deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Employee was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

    public function download() {
        $id = $this->params['pass'];
        $id = $id[0];
        Configure::write('debug', 0);
        $options = array('conditions' => array('Employee.' . $this->Employee->primaryKey => $id));
        $empData = $this->Employee->find('first', $options);
pr($empData); exit;
        header('Content-type: ' . $empData['Employee']['type']);
        header('Content-length: ' . $empData['Employee']['size']);
        header('Content-Disposition: attachment; filename="' . $empData['Employee']['name'] . '"');
        echo $empData['Employee']['document'];
        exit();
    }

}
