<?php
App::uses('AppController', 'Controller');
/**
 * StudentAlotments Controller
 *
 * @property StudentAlotment $StudentAlotment
 */
class StudentAlotmentsController extends AppController {

  
  var $components = array('RequestHandler','Paginator'); 
  var $helpers = array("Html", "Form", "Session");
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->StudentAlotment->recursive = 2;
		$this->set('studentAlotments', $this->paginate());
		$this->Session->write('download_students',$this->paginate());
		
		$this->loadModel('State');
		$this->loadModel('College');
		$this->loadModel('University');
		$this->loadModel('City');
		
		$this->set('states',$this->State->find('list'));
		$this->set('universities',$this->University->find('list',array('order'=>'University.name ASC')));
		$this->set('colleges',$this->College->find('list',array('order'=>'College.name ASC')));
		
		$cities = $this->City->find('list');
		
		$this->set('cities',$cities);
		
		/*Search methods*/
	
		$conditions = array();
		
		if(!empty($this->request->data)  || !empty($this->passedArgs['student_name']) || !empty($this->passedArgs['application_number']) || !empty($this->passedArgs['state_id']) || !empty($this->passedArgs['city_id']) || !empty($this->passedArgs['college_id']) || !empty($this->passedArgs['university_id']) || !empty($this->passedArgs['gender'])){	
			

			if(isset($this->request->data['Reset']))
       		{
       			$this->redirect("/studentAlotments/index");
       			}	
			
			@$this->passedArgs['student_name'] = isset($this->request->data['StudentAlotment']['student_name']) ? $this->request->data['StudentAlotment']['student_name'] : $this->passedArgs['student_name'];
			
			@$this->passedArgs['application_number'] = isset($this->request->data['StudentAlotment']['application_number']) ? $this->request->data['StudentAlotment']['application_number'] : $this->passedArgs['application_number'];
			
			@$this->passedArgs['state_id'] = isset($this->request->data['StudentAlotment']['state_id']) ? $this->request->data['StudentAlotment']['state_id'] : $this->passedArgs['state_id'];
			
			@$this->passedArgs['city_id'] = isset($this->request->data['StudentAlotment']['city_id']) ? $this->request->data['StudentAlotment']['city_id'] : $this->passedArgs['city_id'];
			
			@$this->passedArgs['college_id'] = isset($this->request->data['StudentAlotment']['college_id']) ? $this->request->data['StudentAlotment']['college_id'] : $this->passedArgs['college_id'];
			
			@$this->passedArgs['university_id']  = isset($this->request->data['StudentAlotment']['university_id']) ? $this->request->data['StudentAlotment']['university_id'] : $this->passedArgs['university_id'];
			
			@$this->passedArgs['gender'] = isset($this->request->data['StudentAlotment']['gender']) ? $this->request->data['StudentAlotment']['gender'] : $this->passedArgs['gender'];
			
			
			
			
			if(!empty($this->passedArgs['student_name'])){
			
			$conditions[] =
		"StudentRegistration.applicant_name LIKE '".$this->passedArgs['student_name']."%'";
			
			
			}
			
			if(!empty($this->passedArgs['application_number'])){
			$conditions[] =
		"StudentRegistration.application_number ='".$this->passedArgs['application_number']."'";
			
			}
			
			if(!empty($this->passedArgs['state_id'])){
			$conditions[] =
				"StudentRegistration.state_id ='".$this->passedArgs['state_id']."'";
			
			}
			
			if(!empty($this->passedArgs['city_id'])){
			$conditions[] =
				"StudentRegistration.city_id ='".$this->passedArgs['city_id']."'";
			
			}
			
			if(!empty($this->passedArgs['college_id'])){
			$conditions[] =
				"CollegeGroupSubject.college_id ='".$this->passedArgs['college_id']."'";
			
			}
			
			if(!empty($this->passedArgs['university_id'])){
			
			$Collegelistid = $this->College->find('all',array('fields'=>array('id','university_id'),'conditions'=>array('College.university_id'=>$this->passedArgs['university_id'])));
			$collegeidlist = array();
			$collegelist = '';
			if(!empty($Collegelistid)){
				foreach($Collegelistid as $collegeID){
					$collegeidlist[] = $collegeID['College']['id'];
				}
				$collegelist = implode(',',$collegeidlist);
					
			}
		
			$conditions[] = "CollegeGroupSubject.college_id IN (".$collegelist.")";
		
			}
			
			if(!empty($this->passedArgs['gender'])){
			$conditions[] =
		"StudentRegistration.gender ='".$this->passedArgs['gender']."'";
		
			}
			
		
		$data = $this->StudentAlotment->find('all',array('conditions'=>$conditions,'contains'=>array('StudentRegistration','CollegeGroupSubject')));
		
		$this->Session->write('download_students',$data);
		
		$this->set('studentAlotments',$this->paginate('StudentAlotment',$conditions));
		
		$this->set('universityID',$this->passedArgs['university_id']);
		$this->set('colleges',$this->College->find('list',array('conditions'=>array('College.university_id'=>$this->passedArgs['university_id']),'order'=>'College.name ASC')));
	
		$this->set('cities',$this->City->find('list',array('conditions'=>array('City.state_id'=>$this->passedArgs['state_id']))));
		$this->set('collegeID',$this->passedArgs['college_id']);
		$this->set('stateID',$this->passedArgs['state_id']);
		$this->set('cityID',$this->passedArgs['city_id']);
		$this->set('genderID',$this->passedArgs['gender']);
		
		
		}else{
		//pr($this->paginate()); exit;
		$this->set('studentAlotments', $this->paginate());
		
		}
		
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->StudentAlotment->exists($id)) {
			throw new NotFoundException(__('Invalid student alotment'));
		}
		$options = array('conditions' => array('StudentAlotment.' . $this->StudentAlotment->primaryKey => $id));
		$this->set('studentAlotment', $this->StudentAlotment->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->StudentAlotment->create();
			if ($this->StudentAlotment->save($this->request->data)) {
				$this->Session->setFlash(__('The student alotment has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The student alotment could not be saved. Please, try again.'));
			}
		}
		$studentRegistrations = $this->StudentAlotment->StudentRegistration->find('list');
		$collegeGroupSubjects = $this->StudentAlotment->CollegeGroupSubject->find('list');
		$this->set(compact('studentRegistrations', 'collegeGroupSubjects'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->StudentAlotment->exists($id)) {
			throw new NotFoundException(__('Invalid student alotment'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->StudentAlotment->save($this->request->data)) {
				$this->Session->setFlash(__('The student alotment has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The student alotment could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('StudentAlotment.' . $this->StudentAlotment->primaryKey => $id));
			$this->request->data = $this->StudentAlotment->find('first', $options);
		}
		$studentRegistrations = $this->StudentAlotment->StudentRegistration->find('list');
		$collegeGroupSubjects = $this->StudentAlotment->CollegeGroupSubject->find('list');
		$this->set(compact('studentRegistrations', 'collegeGroupSubjects'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->StudentAlotment->id = $id;
		if (!$this->StudentAlotment->exists()) {
			throw new NotFoundException(__('Invalid student alotment'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->StudentAlotment->delete()) {
			$this->Session->setFlash('Student alotment deleted','default',array('class'=>'successmessage'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Student alotment was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	
	
	public function download_nominated_student(){
			$nominatedstudentDetails = $this->Session->read('download_students');
			//pr($nominatedstudentDetails); exit;
			
			if(!empty($nominatedstudentDetails)){
				$this->set('studentAlotments', $nominatedstudentDetails);
				$year = '2013';
				$this->layout =NULL;
		        header("Content-Type:application/vnd.ms-excel");
		        header("Content-Disposition: attachment;Filename=nominatedstudent_".$year.".xlsx");
            }else{
         	   $this->Session->setFlash(__('No data is there to download'));
            }
	
	}
	
	public function listallocatedpercentage() {
   
            
        $this->StudentAlotment->recursive = 3;
        $db = $this->StudentAlotment->getDataSource();
        
        /**/
        $this->loadModel('College');
		$this->loadModel('University');

		$this->set('universities',$this->University->find('list',array('order'=>'University.name ASC')));
		$this->set('colleges',$this->College->find('list',array('order'=>'College.name ASC')));
            
        $conditions = array();
        if(!empty($this->request->data)){
        	
				if(!empty($this->request->data['StudentAlotment']['college_id'])){
					$conditions[] = "CollegeGroupSubject.college_id ='".$this->request->data['StudentAlotment']['college_id']."'";

				}
				$this->set('collegeid',$this->request->data['StudentAlotment']['college_id']);
        }   
        /**/
        

			$this->paginate = array(
							'conditions'=>$conditions,
							'fields'=>array('max(StudentAlotment.grade) as grade ','StudentAlotment.allocation_year','StudentAlotment.college_group_subject_id',
							'(SELECT total_percentage
							FROM student_registrations
							WHERE id = (select a.student_registration_id from ' . $db->fullTableName($this->StudentAlotment) . ' a WHERE StudentAlotment.college_group_subject_id = a.college_group_subject_id AND a.grade = max(StudentAlotment.grade)))as total_percentage'),
							'group' => array('StudentAlotment.college_group_subject_id'),
							'order' => array('StudentAlotment.allocation_year')
							); 

        		$this->Session->write('alllcatedpercentage',$this->paginate('StudentAlotment'));
               $this->set('listallocatedpercentage',$this->paginate('StudentAlotment'));
               
        
		
               
    }

    public function download_listallocated_percentage(){
            $alllcatedpercentageDetails = $this->Session->read('alllcatedpercentage');

            if(!empty($alllcatedpercentageDetails)){
            $this->set('alllcatedpercentageDetails', $alllcatedpercentageDetails);
            $year = Date('Y');
            $this->layout =NULL;
            header("Content-Type:application/vnd.ms-excel");
            header("Content-Disposition: attachment;Filename=listallocatePercentage_".$year.".xlsx");
            }else{
            $this->flash(__('No data is there to download'), array('action' => 'index'));
            }

    } 
    
    /*Exception report*/
    
    public function exception() {
            $this->StudentAlotment->recursive = 3;
        $db = $this->StudentAlotment->getDataSource();

                $this->paginate = array(
                'conditions'=>null,
                'fields'=>array('StudentAlotment.course_id','StudentAlotment.allocation_year',
                'StudentAlotment.college_group_subject_id',
                'count(StudentAlotment.id) as opt'),

                'group' => array('StudentAlotment.college_group_subject_id',
                'StudentAlotment.course_id'),
                );

          $this->Session->write('exceptionsadmission',$this->paginate('StudentAlotment'));
              $this->set('exceptionallotment',$this->paginate('StudentAlotment'));
    }



    public function download_exceptions(){
            $exceptionsadmissions = $this->Session->read('exceptionsadmission');

            if(!empty($exceptionsadmissions)){
		        $this->set('exceptionsadmissions', $exceptionsadmissions);
		        $year = Date('Y');
		        $this->layout =NULL;
		        header("Content-Type:application/vnd.ms-excel");
		        header("Content-Disposition: attachment;Filename=exceptionAdmission_".$year.".xlsx");
            }else{
        	    $this->flash(__('No data is there to download'), array('action' => 'index'));
            } 
            
     }
     
      public function getcitylist() {
        $this->layout = null;
        $this->loadModel('City');
        $state_id = $this->request->data['State'];
        $cities = $this->City->find('list', array('conditions' => array('state_id' => $state_id, 'City.status' => 'Y')));
        $this->set('cities', $cities);
    }
    
    function getcollegelist(){
   	  $this->layout = null;
        $this->loadModel('College');
        $university_id = $this->request->data['university_id'];
        $colleges = $this->College->find('list', array('conditions' => array('College.university_id' => $university_id)));
        $this->set('colleges', $colleges);
    
    }
     
}
