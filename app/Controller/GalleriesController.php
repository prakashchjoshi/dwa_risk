<?php

App::uses('AppController', 'Controller');


class GalleriesController extends AppController {

    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = array('Html', 'Form', 'Js');

    /**
     * index method
     *
     * @return void
     */
    
      public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('index');
    }
    public function index() {
       
    }
}

    
