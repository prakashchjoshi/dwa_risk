<?php
App::uses('AppController', 'Controller');
/**
 * Gradepoints Controller
 *
 * @property Gradepoint $Gradepoint
 */
class GradepointsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Gradepoint->recursive = 0;
		$this->set('gradepoints', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Gradepoint->exists($id)) {
			throw new NotFoundException(__('Invalid gradepoint'));
		}
		$options = array('conditions' => array('Gradepoint.' . $this->Gradepoint->primaryKey => $id));
		$this->set('gradepoint', $this->Gradepoint->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
		$userid = $this->Session->read('Auth.User.id');
            $this->request->data['Gradepoint']['created_by'] = $userid;
			$this->Gradepoint->create();
			if ($this->Gradepoint->save($this->request->data)) {
				$this->Session->setFlash(__('The gradepoint has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The gradepoint could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Gradepoint->exists($id)) {
			throw new NotFoundException(__('Invalid gradepoint'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
		  $userid = $this->Session->read('Auth.User.id');
            $this->request->data['Gradepoint']['modified_by'] = $userid;
			if ($this->Gradepoint->save($this->request->data)) {
				$this->Session->setFlash(__('The gradepoint has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The gradepoint could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Gradepoint.' . $this->Gradepoint->primaryKey => $id));
			$this->request->data = $this->Gradepoint->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Gradepoint->id = $id;
		if (!$this->Gradepoint->exists()) {
			throw new NotFoundException(__('Invalid gradepoint'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Gradepoint->delete()) {
			$this->Session->setFlash(__('Gradepoint deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Gradepoint was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
